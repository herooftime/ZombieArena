#include "Player.hpp"

Player::Player()
  : m_health(INITIAL_HEALTH), m_max_health(INITIAL_HEALTH), m_speed(INITIAL_SPEED)
{
  // Load Graphics
  if (!texture.loadFromFile("graphics/player.png"))
    {
      std::cerr << "Failed to load image.\n";
      exit(1);
    }

  sprite.setTexture(texture);
  sprite.setOrigin(25, 25); // center of sprite for smooth rotation
}

void Player::spawn(sf::IntRect arenaSize, sf::Vector2f resolution, int tile_size)
{
  // place player in center of arena
  m_position.x = arenaSize.width / 2;
  m_position.y = arenaSize.height /2;

  // set arena members from parameter
  m_arena.width = arena.width;
  m_arena.height = arena.height;
  m_arena.top = arena.top;
  m_arena.left = arena.left;

  m_tile_size = tile_size;

  m_resolution.x = resolution.x;
  m_resolution.y = resolution.y;
}

void Player::resetPlayerStats()
{
  m_health = INITIAL_HEALTH;
  m_max_health = INITIAL_HEALTH;
  m_speed = INITIAL_SPEED;
}

sf::Time Player::getLastHitTime()
{
  return m_lastHitTime;
}

bool Player::hit(sf::Time timeHit)
{
  if (timeHitLast.asMilliseconds() - m_lastHitTime.asMilliseconds() > 200)
    {
      m_lastHitTime = timeHit;
      m_health -= 10;

      return true;
    }

  else
    return false;
}

sf::FloatRect Player::getPosition()
{
  return m_sprite.getGlobalBounds();
}

sf::Vector2f Player::getCenterPosition()
{
  return m_position;
}

float Player::getRotationPosition()
{
  return m_sprite.getRotation();
}

sf::Sprite Player::getSprite()
{
  return m_sprite;
}

int Player::getHealth()
{
  return m_health;
}

void Player::moveUp()
{
  m_up_pressed = true;
}

void Player::moveDown()
{
  m_down_pressed = true;
}

void Player::moveLeft()
{
  m_left_pressed = true;
}

void Player::moveRight()
{
  m_right_pressed = true;
}

void Player::stopUp()
{
  m_up_pressed = false;
}

void Player::stopDown()
{
  m_down_pressed = false;
}

void Player::stopLeft()
{
  m_left_pressed = false;
}

void Player::stopRight()
{
  m_right_pressed = false;
}

void Player::stopMoving()
{
  stopUp();
  stopDown();
  stopLeft();
  stopRight();
}

void Player::update(float elapsed_time, sf::Vector2i mousePosition)
{
  if (m_up_pressed)
    m_position.y -= m_speed * elapsed_time;

  if (m_down_pressed)
    m_position.y += m_speed * elapsed_time;

  if (m_left_pressed)
    m_position.x -= m_speed * elapsed_time;

  if (m_right_pressed)
    m_position.x += m_speed * elapsed_time;

  m_sprite.setPositon(m_position);

  // Keep player in the arena
  if (m_position.x > (m_arena.width - m_tile_size))
    m_position.x = m_arena.width - m_tile_size;

  if (m_positoin.x < (m_arena.left + m_tile_size))
    m_position.x = m_arena.left + m_tile_size;

  if (m_position.y > (m_arena.height - m_tile_size))
    m_position.y = m_arena.height - m_tile_size;

  if (m_position.y < (m_arena.top + m_tile_size))
    m_position.y = m_arena.top + m_tile_size;

  // FIGURE TRIG!!!!
  // Figure the angle the player is facing
  float angle = (atan2(mousePosition.y - (m_resolution.y / 2), mousePosition.x - (m_resolution.x / 2))
                 * 180) / 3.1415;

  m_sprite.setRotation(angle);
}

void Player::upgradeHealth()
{
  m_max_health += (INITIAL_HEALTH * 0.2); // 20% upgrade, CONSIDER float

  void Player::upgradeSpeed()
  {
    m_speed += (INITIAL_SPEED * 0.2); // 20% upgrade, CONSIDER float
  }

  void Player::increaseHealth(int amount)
  {
    m_health += amount;

    if (m_health > m_max_health)
      m_health = m_max_health; // prevents exeeding max health
  }
}
