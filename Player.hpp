#ifndef PLAYER_HPP
#define PLAYER_HPP

#include <SFML/Graphics.hpp>

class Player
{
private:
  const float INITIAL_SPEED = 200;
  const float INITIAL_HEALTH = 100;

  sf::Vector2f m_position;
  sf::Texture m_texture;
  sf::Sprite m_sprite;
  sf::Vector2f m_resolution;
  sf::IntRect m_arena;
  sf::Time m_lastHitTime;

  int m_tile_size;

  // Player Direction
  bool m_left_pressed;
  bool m_right_pressed;
  bool m_up_pressed;
  bool m_down_pressed;

  // PLayer Stats
  int m_health;
  int m_max_health;
  float m_speed;

public:
  Player();

  void spawn(sf::IntRect m_arena, sf::Vector2f m_resolution, int m_tile_size);

  void resetPlayerStats();

  bool hit(Time timeHit);

  // Accessors

  sf::Time getLastHitTime();

  sf::FloatRect getPosition();

  sf::Vector2f getCenterPosition();

  float getRotationPosition();

  sf::Sprite getSprite();

  int getHealth();

  // Move the Player

  void moveUp();
  void moveDown();
  void moveLeft();
  void moveRight();

  void stopUp();
  void stopDown();
  void stopLeft();
  void stopRight();
  void stopMoving();

  // Screen Functions

  void updateFrame(float elapsedTime, sf::Vector2i mousePosition);

  // Upgrade Stat Functions

  void upgradeHealth();

  void upgradeSpeed();

  void increaseHealth(int amount);
};

#endif // PLAYER_HPP
