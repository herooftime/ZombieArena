# ZombieArena

This repository contains code created with the help of tutorial, but with
plans to ultimately change the project. The graphics were received from the
tutorial as well. This may also be changed in the future.

## Contact

mr.carpinelli@protonmail.ch

## Contributing

This is mostly a personal project, but any contributions are welcome and
appreciated.

## Building

SFML is required.

## License

All contributions are made under the [GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html). See the [LICENSE](LICENSE).
