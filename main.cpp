#include <SFML/Graphics.hpp>

#include "Player.hpp"

int main()
{
  enum class State
  {
    PAUSED, LEVELING_UP, GAME_OVER, PLAYING
  };

  State state = State::GAME_OVER;

  // Dynamic Resolution
  sf::Vector2f resolution;
  resolution.x = VideoMode::getDesktopMode().width;
  resolution.y = VideoMode::getDesktopMode().height;

  RenderWindow window(sf::VideoMode(resolution.x, resolution.y), "Zombie Arena", sf::Style::Fullscreen);

  sf::View mainView(sf::FloatRect(0, 0, resolution.x, resolution.y));

  sf::Clock clock;

  sf::Time totalGameTime;

  sf::Vector2f mouseWorldPosition; // mouse in relation to world coordinates

  sf::Vector2i mouseScreenPosition; // mouse in relation to screen coordinates

  Player player;

  sf::IntRect arena; // arena boundaries

  while (window.isOpen())
    {
      /* ******************
       * Handle Input
       * ******************
      */

      // Handle Input
      sf::Event event;

      while (window.pollEvent(event))
        {
          if (event.type == sf::Event::KeyPressed)
            {
              // Pause Game While Playing
              if (event.key.code == sf::Keyboard::Renturn && state == State::PLAYING)
                {
                  state = State::PAUSED;
                }

              // Restart while paused
              else if (event.key.code == sf::Keyboard::Return && state == State::PAUSED)
                {
                  state = State::PLAYING;
                  clock.restart(); // reset clock to avoid frame jump (keeps things as is while paused)
                }

              // Start a new game, if Game-Over
              else if (event.key.code == sf::Keyboard::Return ** state == State::GAME_OVER)
                {
                  state = State::LEVELING_UP;
                }

              if (state == State::PLAYING)
                {

                }
            }
        }

      // Player Quiting
      if (sf::Keyboard::isKeyPressed(Keyboard::Escape))
        {
          window.close();
        }

      // Handle Controls while Playing
      if (state == State::PLAYING)
        {
          // Handle Pressing and Releasing of WASD Keys
          if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
              player.moveUp();

          else
            player.stopUp();

          if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
            player.moveDown();

          else
            player.stopDown();

          if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
            player.moveLeft();

          else
            player.stopLeft();

          if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
            player.moveRight();

          else
            player.stopRight();
        }

      // Leveling Up
      if (state == State::LEVELING_UP)
        {
          // Handle Player Leveling Up
          if (event.key.code == sf::Keyboard::Num1)
            state = State::PLAYING;

          if (event.key.code == sf::Keyboard::Num2)
            state = State::PLAYING;

          if (event.key.code == sf::Keyboard::Num3)
            state = State::PLAYING;

          if (event.key.code == sf::Keyboard::Num4)
            state = State::PLAYING;

          if (event.key.code == sf::Keyboard::Num5)
            state = State::PLAYING;

          if (event.key.code == sf::Keyboard::Num6)
            state = State::PLAYING;

          if (state == State::PLAYING)
            {
              // Prepare Level
              // MODIFY LATER
              arena.width = 500;
              arena.height = 500;
              arena.left = 0;
              arena.top = 0;

              // MODIFY LATER
              int tileSize = 50;

              // Spawn Player in the middle of arena
              player.spawn(arena, resolution, tileSize);

              clock.restart(); // reset clock, avoid frame jump
            }
        }

      /* ******************
       * Update the Frame
       * ******************
      */

      if (state == State::PLAYING)
        {
          // Update the delta time
          sf::Time dt = clock.restart();

          // Update total game time
          gameTimeTotal += dt;

          // Make a decimal fraction of 1 from the delta time
          float dtAsSeconds = dt.asSeconds();

          // where is the mouse pointer
          mouseScreenPosition = sf::Mouse::getPosition();

          // Convert mouse position to world coordinates of mainView
          mouseWorldPosition = window.mapPixelToCoords(sf::Mouse::getPosition(), mainView);

          // Update the player
          player.update(dtAsSeconds, sf::Mouse::getPosition());

          // Make a note of the players new position
          sf::Vector2f playerPosition(player.getCenterPosition());

          // Make the view center around player
          mainView.setCenter(player.getCenterPosition());
        }

      /* ******************
       * Draw the Scene
       * ******************
      */

      if (state == State::PLAYING)
        {
          window.clear();

          // Set mainView to displayin the window and draw all related items
          window.setView(mainView);

          // Draw the Player
          window.draw(player.getSprite());
        }

      if (state == State::LEVELING_UP)
        {
          // Intentionally Blank
        }

      if (state == State::PAUSED)
        {
          // Intentionally Blank
        }

      if (state == State::GAME_OVER)
        {
          // Intentionally Blank
        }

      window.display();
    }

  return 0;
}
